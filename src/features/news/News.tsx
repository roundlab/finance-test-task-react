import { useState, useEffect } from "react";
import styles from "./News.module.scss";

interface Post {
  title: string;
  description: string;
  urlToImage: string;
  publishedAt: Date;
}

export function News() {
  const newsLimitPerLoad = 5;
  const [error, setError] = useState<any>(null);
  const [isLoaded, setIsLoaded] = useState<boolean>(false);

  const [items, setItems] = useState<Post[]>([]);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [totalCount, setTotalCount] = useState<number>(0);

  useEffect(() => {
    fetch(
      `https://my-json-server.typicode.com/kamilkn/demo1/news?_limit=${newsLimitPerLoad}&_page=${currentPage}`
    )
      .then((res) => {
        setTotalCount(Number(res.headers.get("x-total-count")));
        return res.json();
      })
      .then(
        (response: Post[]) => {
          setIsLoaded(true);
          setItems([...items, ...response]);
        },
        // Примечание: важно обрабатывать ошибки именно здесь, а не в блоке catch(),
        // чтобы не перехватывать исключения из ошибок в самих компонентах.
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      );
    // eslint-disable-next-line
  }, [currentPage]);

  if (error) {
    return <div>Ошибка: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Загрузка...</div>;
  } else {
    return (
      <div>
        <ul className={styles.news}>
          {items
            .sort((post1, post2) => {
              return post1.publishedAt < post2.publishedAt ? 1 : -1;
            })
            .map((item, key) => (
              <li key={key} className={styles.item}>
                {item.urlToImage && <img src={item.urlToImage} alt="" />}
                <h3>{item.title}</h3>
                <p>{item.description}</p>
                <p>{item.publishedAt}</p>
              </li>
            ))}
        </ul>
        {items.length < totalCount && (
          <button
            className={styles.button}
            onClick={() => setCurrentPage(currentPage + 1)}
          >
            Show more
          </button>
        )}
      </div>
    );
  }
}
