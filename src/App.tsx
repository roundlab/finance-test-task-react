import { News } from './features/news/News';
import './App.scss';

function App() {
  return (
    <div className="App">
        <News />
    </div>
  );
}

export default App;
